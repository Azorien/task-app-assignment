package com.example.azorien.taskapp;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.azorien.taskapp.database.entity.Task;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TaskListAdapter recyclerViewAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private Task[] dataset = new Task[0];
    private int activeID = -1;
    private int activeState = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final SharedPreferences preferences = App.INSTANCE.getPreferences();
        new Thread(new Runnable() {
            @Override
            public void run() {
                // if first launch fill the database up
                if(preferences.getBoolean(App.PREF_FIRST_LAUNCH, true)){
                    App.INSTANCE.getDatabase().taskDao().insertAll(generateTasks(20));
                    preferences.edit().putBoolean(App.PREF_FIRST_LAUNCH, false).commit();
                }

                // get data
                dataset = App.INSTANCE.getDatabase().taskDao().getAll();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recyclerViewAdapter.setData(dataset);
                        recyclerViewAdapter.notifyDataSetChanged();
                    }
                });


            }
        }).start();

        recyclerViewAdapter = new TaskListAdapter(dataset);
        layoutManager = new LinearLayoutManager(this);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerViewAdapter);

        recyclerViewAdapter.setActiveID(App.INSTANCE.getPreferences().getInt(App.PREF_ACTIVE_ID, -1));
        recyclerViewAdapter.setActiveState(App.INSTANCE.getPreferences().getInt(App.PREF_ACTIVE_STATE, 0));

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerViewAdapter.setButtonCLickedListener(new TaskListAdapter.ButtonClickedListener() {
            @Override
            public void onItemClicked(int id) {
                activeState++;
                if(activeState > 2){
                    activeState = 0;
                    activeID = -1;
                }
                else{
                    activeID = id;
                }

                recyclerViewAdapter.setActiveID(activeID);
                recyclerViewAdapter.setActiveState(activeState);
                recyclerViewAdapter.notifyDataSetChanged();
                App.INSTANCE.getPreferences().edit().putInt(App.PREF_ACTIVE_ID, activeID).apply();
                App.INSTANCE.getPreferences().edit().putInt(App.PREF_ACTIVE_STATE, activeState).commit();
            }
        });



    }

    private Task[] generateTasks(int amount){
        Task[] tasks = new Task[amount];

        for (int i = 0; i<amount; i++){
            tasks[i] = new Task();
            tasks[i].setId(i);
            tasks[i].setName("Task " + i);
        }

        return tasks;
    }

}
