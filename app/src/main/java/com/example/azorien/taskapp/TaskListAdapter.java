package com.example.azorien.taskapp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.azorien.taskapp.database.entity.Task;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.TaskViewHolder> {
    private Task[] dataset;
    private int activeID = -1;
    private int activeState = 0;

    public int getActiveID() {
        return activeID;
    }

    public void setActiveID(int activeID) {
        this.activeID = activeID;
    }

    public int getActiveState() {
        return activeState;
    }

    public void setActiveState(int activeState) {
        this.activeState = activeState;
    }

    private ButtonClickedListener listener;

    public TaskListAdapter(Task[] dataset){
        this.dataset = dataset;
    }

    public void setButtonCLickedListener(ButtonClickedListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LinearLayout view = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_item, viewGroup, false);
        return new TaskViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder viewHolder, int i) {
        viewHolder.name.setText(dataset[i].getName());
        viewHolder.id.setText(String.valueOf(dataset[i].getId()));
        viewHolder.base.setBackgroundResource(R.color.taskOpen);

        if(activeID == dataset[i].getId()){
            viewHolder.button.setVisibility(View.VISIBLE);
            if(activeState == ListTask.STATE_TRAVEL){
                viewHolder.state.setText(R.string.state_travel);
                viewHolder.button.setText(R.string.button_work);
                viewHolder.base.setBackgroundResource(R.color.taskTravel);
            }
            if(activeState == ListTask.STATE_WORK){
                viewHolder.state.setText(R.string.state_work);
                viewHolder.button.setText(R.string.button_stop);
                viewHolder.base.setBackgroundResource(R.color.taskWork);
            }
        }
        else if (activeID >= 0) {
            viewHolder.button.setVisibility(View.INVISIBLE);
            viewHolder.state.setText(R.string.state_open);
        }
        else {
            viewHolder.button.setVisibility(View.VISIBLE);
            viewHolder.button.setText(R.string.button_start);
            viewHolder.state.setText(R.string.state_open);
        }
    }

    @Override
    public int getItemCount() {
        return dataset.length;
    }

    public void setData(Task[] dataset) {
        this.dataset = dataset;
    }


    public class TaskViewHolder extends RecyclerView.ViewHolder{

        public TextView name;
        public TextView id;
        public TextView state;
        public Button button;
        public View base;

        public TaskViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            id = itemView.findViewById(R.id.id);
            state = itemView.findViewById(R.id.state);
            base = itemView;
            button = itemView.findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onItemClicked(dataset[getAdapterPosition()].getId());
                    }
                }
            });
        }
    }

    public interface ButtonClickedListener {
        void onItemClicked(int id);
    }
}
