package com.example.azorien.taskapp;

public class ListTask {
    public static final int STATE_OPEN = 0;
    public static final int STATE_TRAVEL = 1;
    public static final int STATE_WORK = 2;

    private final int id;
    private final String name;

    public ListTask(int id, String name, int state){
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
