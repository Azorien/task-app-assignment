package com.example.azorien.taskapp;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.azorien.taskapp.database.TaskDatabase;
import com.example.azorien.taskapp.database.entity.Task;

public class App extends Application {
    public static App INSTANCE;

    public static final String DATABASE_NAME = "TaskDatabase";
    public static final String PREFERENCES = "TaskApp";
    public static final String PREF_FIRST_LAUNCH = "firstLaunch";
    public static final String PREF_ACTIVE_ID = "activeID";
    public static final String PREF_ACTIVE_STATE = "activeState";

    private TaskDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();

        database = Room.databaseBuilder(getApplicationContext(), TaskDatabase.class, DATABASE_NAME)
                .build();

        INSTANCE = this;
    }

    public TaskDatabase getDatabase() {
        return database;
    }

    public SharedPreferences getPreferences(){
        return getSharedPreferences(PREFERENCES, MODE_PRIVATE);
    }
}
