package com.example.azorien.taskapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.azorien.taskapp.database.entity.Task;

@Dao
public interface TaskDao {
    @Query("SELECT * FROM Task")
    Task[] getAll();

    @Insert
    void insertAll(Task[] tasks);
}
